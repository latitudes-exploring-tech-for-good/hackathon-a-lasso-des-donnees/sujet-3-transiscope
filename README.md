# Sujet n°3 : visualisation d'evenement (design et prototype)   - Transiscope (https://transiscope.org/).

+ *Phase d'avancement actuelle :* Evolution prioritaire à apporter au projet existant de Transicsope.
+ *Compétences associés :* design (wireframe, graphistes, design thinking, web design...), developpement web (javascript, HTML, web component..), gestion de projet (product owner, agilité...), traitement de donnée.

##### #0 | Résultats obtenus lors du hackathon "À l'asso des données"
Cette section contiendra des informations sur les résultats obtenus dans le cadre du hackathon "À l'asso des données".

##### #1 | Présentation de Transiscope
Aujourd’hui, de nombreux d’acteurs de la transition et des alternatives ont entamé un travail de recensement et de cartographie de leurs organisations, actions et écosystèmes.
Dans la majorité des cas, néanmoins, ces informations sont éparpillées sur les sites de chacune de ces organisations et les données ne peuvent pas communiquer entre elles en raison de choix techniques différents : aucune visualisation agrégée n’était jusqu’à présent possible.
Pour permettre de relier ces alternatives, une dizaine de collectifs travaillent depuis deux ans pour développer des outils libres permettant de connecter les différentes bases de données existantes et de les visualiser au même endroit : TRANSISCOPE.

+ https://transiscope.org/qui-sommes-nous/
+ présentation fonctionnelle : https://docs.google.com/presentation/d/e/2PACX-1vT2B2hBcmM5kkQjOVqnJ8C94dc5dATWT5OSzMYW9lCbBgpG5Hvx-xQCTgT4mMP9KJ8-oKCltuWVBEvn/pub?start=false&loop=false&delayms=3000
+ présentation technique : https://docs.google.com/presentation/d/e/2PACX-1vSEN2d7i3xEfoPiUs2fgDwhT1I7zsp7MRwo5a8I4Tcqqgp1djWqHaQH4TlBAlRHRA/pub?start=false&loop=false&delayms=3000
+ 22 000 alternatives
+ 12 Collectifs au Copil
+ 23 Sources de données
+ 120 000 Visiteurs depuis l'ouverture
+ 250 Visiteurs / jours

##### #2 | Problématique
Le transiscope est en train de trouver son rythme de croisière et continue de grandir en terme de referencement (de nouvelles sources de donnée). Le Collectif souhaite ne plus se limiter aux alternatives installées sur les territoires mais référencer également les evénements. 
C'est une double problématique car les évènenements ont un taux de rafraichissement plus élevées que les alternatives sur le territore et que la cartographie actuelle ne suffit pas pour représenter/rechercher/être informé correctement des evenements.

##### #3 | Le défi proposé
Le défi proposé concerne essentiellement la partie représentation graphique bien que le traitement de données puisse être abordé. Nous proposons aux participants de proposer des designs et réalisent des prototypes d'interface graphique les plus adaptées possibles aux événements tout en tenant compte de l'historique, des choix technologiques déjà réalisés et des partenariats de Transiscope. C'est un défi complet qui va du brainstorming au logiciel clef en main en passant par un processus créatif et du pilotage. 
Les travaux seront tous en licence Open Source (CC BY). Plusieurs équipes peuvent être créées et le mode d'organisation est libre bien que nous recommandions fortement l'agilité.

Transiscope dispose déjà d'un outil de traitement de donnée (Bus Sémantique) et de représentation cartographique (GoGoCarto). Des données seront à disposition des participants (Agenda Comme un Nantes, Communecter, Mobilizon, Demosphere, etc) et le mentor sera garant de fournir des données mais les participants sont les bienvenus pour découvrir comment cela fonctionne et contribuer sur cette partie.

##### #4 | Livrables
Dans l'ordre aproximatif d'aboutissement :
+ Concept
+ Wireframe
+ Mockup - Maquettes
+ Prototype
+ Web component réutiliseable

##### #5 | Ressources à disposition pour résoudre le défi

+ Un mentor, architecte/developpeur senior ayant une tres bonne connaisance du projet et de l'écosystème : Simon Louvet.
+ Des wireframes déjà produites par les referents techniques du projet : Sebatian Castro et Simon Louvet.
    + https://drive.google.com/file/d/1UWtPGXlma9D8ukKCHx-H1PQp2-eEjZIV/view
+ Des données pour faire des tests.
    + https://grappe.io/data/api/5c961ef44a490f002e4598f6-agenda-comme-un-nantes-events-all
+ Des exemples d'applications qui représentevisualisent des évènements.
    + mobilizon : https://test.mobilizon.org/
    + demosphere : https://paris.demosphere.net/
    + agenda comme un : https://agenda-comme-un-nantes.org/
        + https://gitlab.com/agenda-comme-un-nantes/agenda-comme-un-nantes.gitlab.io
    + communecter : https://www.communecter.org/
    + facebook
    + google agenda
    + framagenda : https://framagenda.org/


##### #6 | Code de conduite et philosophie du hackathon
Lors de la conception du hackathon, Latitudes a voulu prendre des partis-pris afin de rendre celui-ci particulier. 
Il s’agit d’éléments que nous souhaitons incarner collectivement avec vous tout au long des 24h :
+ La force du collectif pour faire émerger des solutions adaptées aux porteur.se.s de projets, notamment via la session de stand-up meeting ;
+ Une attention portée à la conception rapide, au test et à l’itération qui doivent permettre aux porteur.se.s de projets d’avoir accès aux solutions les plus abouties possibles ;
+ L’engagement sur des solutions ouvertes, facilement utilisables par les porteur.se.s de projets, et qui vous permettront de continuer à contribuer à l’issue du hackathon si vous le souhaitez, ou de permettre à d’autres personnes de le faire.

##### #7 | Points de contact lors du hackathon
+ Simon Louvet
